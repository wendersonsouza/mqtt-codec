name := "mqtt-codec"

version := "0.1.0-ALPHA1"

scalaVersion := "2.11.6"

resolvers ++= {
  Seq(
    "typesafe" at "http://repo.typesafe.com/typesafe/releases/"
  )
}

libraryDependencies ++= {
  Seq(
    "junit" % "junit" % "4.10",
    "org.scodec" %% "scodec-core" % "1.7.1",
    "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"
  )
}
