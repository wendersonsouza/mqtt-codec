package br.com.trustsystems.mqtt

import scodec.Codec
import scodec.bits._
import scodec.codecs._

package object codec {
  val protocolName: BitVector = variableSizeBytes(uint16, utf8).encode("MQTT").require
  val protocolLevel: BitVector = uint8.encode(4).require

  val dupCodec: Codec[Boolean] = bool
  val qosCodec: Codec[Short] = ushort(2)
  val retainCodec: Codec[Boolean] = bool


  val userNameFlagCodec: Codec[Boolean] = bool
  val passwordFlagCodec: Codec[Boolean] = bool
  val willRetainCodec: Codec[Boolean] = bool
  val willQosCodec: Codec[Short] = ushort(2)
  val willFlagCodec: Codec[Boolean] = bool
  val cleanSessionCodec: Codec[Boolean] = bool
  val keepAliveCodec: Codec[Int] = uint16
  val connectAcknowledgeCodec: Codec[Unit] = constant(bin"0000000")
  val sessionPresentFlagCodec: Codec[Boolean] = bool
  val clientIdCodec: Codec[String] = variableSizeBytes(uint16, utf8)
  val topicCodec: Codec[String] = variableSizeBytes(uint16, utf8)
  val messageCodec: Codec[ByteVector] = variableSizeBytes(uint16, bytes)
  val userCodec: Codec[String] = variableSizeBytes(uint16, utf8)
  val passwordCodec: Codec[String] = variableSizeBytes(uint16, utf8)
  val returnCodeCodec: Codec[Short] = ushort(8)
  val packetIdCodec: Codec[Int] = uint16
  val remainingLengthCodec = new RemainingLength
  val payloadCodec: Codec[ByteVector] = bytes
  val subscribeTopicFilterCodec: Codec[List[(String, Short)]] = list((topicCodec :: ignore(6) :: qosCodec).dropUnits.as[(String, Short)])
  val unsubscribeTopicFilterCodec: Codec[List[String]] = list(topicCodec)
  val topicReturnCodeCodec: Codec[List[Short]] = list(ushort(8))

  val fixedHeaderCodec: Codec[FixedHeader] = (dupCodec :: qosCodec :: retainCodec).as[FixedHeader]
  val connectVariableHeaderCodec: Codec[ConnectVariableHeader] = (
    constant(protocolName) :~>: constant(protocolLevel) :~>:
      userNameFlagCodec ::
      passwordFlagCodec ::
      willRetainCodec ::
      willQosCodec ::
      willFlagCodec ::
      cleanSessionCodec ::
      ignore(1) :~>:
      keepAliveCodec).as[ConnectVariableHeader]
}
